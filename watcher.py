import subprocess, os, time, fcntl

from dotenv import load_dotenv
# API reference - https://pythonhosted.org/watchdog/api.html
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler

#  Python non-blocking read with subprocess.Popen - https://gist.github.com/sebclaeys/1232088
def non_block_read(output):
    fd = output.fileno()
    fl = fcntl.fcntl(fd, fcntl.F_GETFL)
    fcntl.fcntl(fd, fcntl.F_SETFL, fl | os.O_NONBLOCK)
    try:
        return output.read()
    except:
        return ""

# traído de https://www.geeksforgeeks.org/create-a-watchdog-in-python-to-look-for-filesystem-changes/
# en combinación con https://github.com/lazywei/psync/blob/master/psync/watcher.py
class WatchEventHandler(PatternMatchingEventHandler):
    def __init__(self, state, **kwargs):
        super(WatchEventHandler, self).__init__(**kwargs)
        # A dirty way to emit changes to the outside world.
        # Should try to use other pure way to do this.
        self.state = state

    def on_any_event(self, event):
        super(WatchEventHandler, self).on_any_event(event)
        self.state["dirty"] = True

# cargamos el .env
load_dotenv(verbose=True)

# sacado de https://github.com/lazywei/psync/blob/master/psync/cli.py#L105
# vía https://www.programcreek.com/python/example/83969/watchdog.observers.Observer
state = {"dirty": False}
event_handler = WatchEventHandler(state, patterns=['*.py'], ignore_directories=False, case_sensitive=False)
observer = Observer()
observer.schedule(event_handler, path='.', recursive=True)
print('Comenzando watch')
observer.start()

def open_bot_proc():
    # le pasamos el environment tal cual lo tenemos (env=os.environ)
    # le pasamos -u a python así flushea siempre el output
    return subprocess.Popen(['python3', '-um', 'bot'], env=os.environ, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

print('Ejecutando proceso del bot')
bot_proc = open_bot_proc()
try:
    while True:
        # chequeamos cambios
        if state["dirty"]:
            print("Modificación detectada, recargando el bot")
            print('Cerrando proceso del bot')
            bot_proc.terminate()
            print('Re-ejecutando proceso del bot')
            bot_proc = open_bot_proc()
            state["dirty"] = False

        # chequeamos estado del proceso
        if bot_proc.returncode != None:
            print(f'El proceso del bot cerró {"exitosamente" if bot_proc.returncode == 0 else "con código de error #" + str(bot_proc.returncode)}')
            break

        # leemos stdout y stderr del proceso y devolvemos en pantalla
        stdout = non_block_read(bot_proc.stdout)
        if stdout is not None:
            print(stdout.decode('utf'), end='')
        stderr = non_block_read(bot_proc.stderr)
        if stderr is not None:
            print(stderr.decode('utf'), end='')

        # cooldown entre intentos de lectura
        time.sleep(0.5)
except KeyboardInterrupt:
    pass

# cerramos bien todo
print('Cerrando proceso del bot')
bot_proc.terminate()
print('Cerrando watch')
observer.stop()
observer.join()
