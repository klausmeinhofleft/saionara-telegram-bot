__Bot de Telegram que manda mensajes semanales a un grupo seleccionado.__

### Funcionalidades

El bot permite que le carguen múltiples mensajes con formato y links. Después permite elegir qué
días de la semana querés que envíe los mensajes y en qué horario. Los mensajes van ciclando
automáticamente. Además podés agregar/eliminar admins del bot y setear el grupo al que enviará
los mensajes.

Los comandos (en formato para pasarle a [@BotFather](https://telegram.me/botfather)) son:
> start - Comando de bienvenida con listado de comandos   
> estado - Estado del bot   
> mensajes_semanales - Administrar textos de los mensajes semanales   
> dias_horario - Administrar días y horario de los mensajes semanales   
> mensaje_directo - Enviar un mensaje directo   
> admins - Administrar admins del bot   
> setear_grupo - Setear el grupo al que manda los mensajes   
> activar - Activar el bot   
> desactivar - Desactivar el bot   
> cancelar - Cancela el comando actual   
> debug - Datos de debug del bot   

Viene con un watch-er de cambios de código para recargar el bot automáticamente.

### Instalación

1. Instalar requerimientos de python haciendo `source install-requirements.sh`
1. Activar el venv si no estaba activado `source venv/bin/activate`
1. Copiar `ejemplo.env` a `.env` y cambiar variables acorde a su caso
1. Ejecutar `python3 bot.py`, o `python3 watcher.py` si lo desean correr con
en modo recarga automática de cambios.

### Configuración

Los parámetros iniciales de la DB se pueden ver en `db.py` en la función `recreate`. Ahí está __lxs usuarixs admins por defecto__, el texto y grupo por defecto del mensaje.

Variables del `.env`:
- `DB_FILE` es el nombre del archivo de la base de datos Sqlite3 (tiene que ser la misma que se usó al correr `db-create.py`)
- `BOT_TOKEN` es el token del bot, en el formato NÚMERO:LETRAS_Y_SÍMBOLOS
- `LOGGING` settea el logging interno: 1 o yes para loggear, 0 o no para no loggear

Si en algún momento desean reiniciar la base de datos simplemente borrar su archivo.

### Docker

Para correr en docker primero build-ear la imagen:

`docker build -t telegram-bot-mensajeador .`

Y después correrla pasándole los parámetros correspondientes:

`docker run --rm -eDB_FILE=labase.db -eBOT_TOKEN=bot_token
-eLOGGING=1 telegram-bot-mensajeador`

## Persistencia

Para hacerle la base de datos persistente es medio quilombo.
Para no poluir tu instalación de docker se puede agregar el siguiente parámetro
al principio del comando anterior (después del `run`): `-v "$(pwd)/docker_data:/code/db"`

Además hay que agregarle el directorio `db/` al parámetro `DB_FILE`, pasando de `DB_FILE=labase.db` a `DB_FILE=db/labase.db`.

La intrucción completa quedaría:

`docker run -v "$(pwd)/docker_data:/code/db" --rm -eDB_FILE=db/labase.db -eBOT_TOKEN=bot_token
-eLOGGING=1 telegram-bot-mensajeador`
