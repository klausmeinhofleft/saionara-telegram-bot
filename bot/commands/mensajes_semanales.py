from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import (
    CommandHandler,
    CallbackQueryHandler,
    ConversationHandler,
    CallbackContext,
    MessageHandler,
    Filters,
)

from bot.constants import CONVERSATIONS
from bot.db import get_msg_guita, Texto
from bot.job import restart_msg_job
from bot.utils import restricted, escape_markdown_entities, remove_markdown_entities
from .cancel import cancel

'''Layout:
~MAIN Sobre qué mensaje querés operar?
| Mensaje 1 bla bl... | --> ~MENSAJE
| bla blabsadd dxs... |
| Agregar nuevo mensaje | --> ~AGREGAR
| Terminar |
~MENSAJE Qué querés hacer con este mensaje?
| Editar | Eliminar |
| << Volver | Terminar |
~AGREGAR Enviame el texto del nuevo mensaje (o cancelá con /cancelar):
'''

MENSAJE_ENTRY, MENSAJE_OPERAR, MENSAJE_AGREGAR, MENSAJE_EDITAR = CONVERSATIONS.create_consts(4)
# códigos de botones. le ponemos namespace casero con "_"
OPERAR, AGREGAR, VER, ELIMINAR, EDITAR, SETTEAR_PROXIMO, RETURN, END = [f'{MENSAJE_ENTRY}_{i}' for i in range(8)]

COMMAND_SLASH='mensajes_semanales'
def create_handler():
    return ConversationHandler(
        entry_points=[CommandHandler(COMMAND_SLASH, entry_point)],
        states={
            MENSAJE_ENTRY: [
                CallbackQueryHandler(operar_mensaje, pattern='^' + OPERAR + '-[0-9]+$'),
                CallbackQueryHandler(agregar_mensaje, pattern='^' + AGREGAR + '$'),
            ],
            MENSAJE_OPERAR: [
                #CallbackQueryHandler(operar_mensaje, pattern='^' + VER + '$'),
                CallbackQueryHandler(editar_mensaje, pattern='^' + EDITAR + '$'),
                CallbackQueryHandler(eliminar_mensaje, pattern='^' + ELIMINAR + '$'),
                CallbackQueryHandler(settear_proximo_mensaje, pattern='^' + SETTEAR_PROXIMO + '$'),
            ],
            MENSAJE_AGREGAR: [
                MessageHandler((Filters.text & ~Filters.command), get_agregar_mensaje),
            ],
            MENSAJE_EDITAR: [
                MessageHandler((Filters.text & ~Filters.command), get_editar_mensaje),
            ]
        },
        fallbacks=[
            CallbackQueryHandler(entry_point, pattern='^' + RETURN + '$'),
            CallbackQueryHandler(end, pattern='^' + END + '$'),
            CommandHandler('cancelar', cancel),
        ],
    )

def end(update, context):
    # CallbackQuery - https://python-telegram-bot.readthedocs.io/en/stable/telegram.callbackquery.html
    query = update.callback_query
    query.answer()
    query.edit_message_text('❇️ Operación finalizada')
    return ConversationHandler.END

def common_ending_keyboard():
    return [
        InlineKeyboardButton("<< Volver", callback_data=RETURN),
        InlineKeyboardButton("Terminar", callback_data=END),
    ]

@restricted
def entry_point(update, context):
    # InlineKeyboardButton -  https://python-telegram-bot.readthedocs.io/en/stable/telegram.inlinekeyboardbutton.html
    keyboard = []

    msg = get_msg_guita()
    if msg.textos:
        for i, t in enumerate(msg.textos):
            button_text = f'> {t.contenido[:20]}...' if len(t.contenido) > 20 else f'> {t.contenido}'
            # en callback_data insertamos el número del mensaje después del guión
            keyboard.append([InlineKeyboardButton(remove_markdown_entities(button_text), callback_data=f'{OPERAR}-{i}')])

    keyboard.append([InlineKeyboardButton("Agregar nuevo mensaje", callback_data=AGREGAR)])
    keyboard.append([InlineKeyboardButton("Terminar", callback_data=END)])

    reply_markup = InlineKeyboardMarkup(keyboard)
    reply_text = '⏺ Sobre qué mensaje querés operar?'

    # diferenciamos si viene por entry point o por un "volver"
    if update.message:
        update.message.reply_text(reply_text, reply_markup=reply_markup)
    else:
        query = update.callback_query
        query.answer()
        query.edit_message_text(reply_text, reply_markup=reply_markup)

    return MENSAJE_ENTRY

@restricted
def operar_mensaje(update, context):
    query = update.callback_query

    # el número del mensaje viene después del guión (desde callback_data)
    mensaje_i = int(query.data.split('-')[1])

    # guardamos el número del mensaje en el data store interno del chat para levantarlo luego
    # Storing bot, user and chat related data - https://github.com/python-telegram-bot/python-telegram-bot/wiki/Storing-bot,-user-and-chat-related-data
    context.chat_data['mensaje_i'] = mensaje_i

    keyboard = [
        [
            #InlineKeyboardButton("Ver", callback_data=VER),
            InlineKeyboardButton("Editar", callback_data=EDITAR),
            InlineKeyboardButton("Eliminar", callback_data=ELIMINAR),
        ],
        [InlineKeyboardButton("Setear como próximo a mandar", callback_data=SETTEAR_PROXIMO)],
        common_ending_keyboard()
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)

    msg = get_msg_guita()
    reply_text = '⏺ Qué operación querés realizar sobre este mensaje?'
    reply_text = reply_text + f'\n{msg.textos[mensaje_i].contenido}'

    query = update.callback_query
    query.answer()
    query.edit_message_text(reply_text, parse_mode='MarkdownV2', reply_markup=reply_markup)

    return MENSAJE_OPERAR

@restricted
def agregar_mensaje(update, context):
    query = update.callback_query
    query.answer()
    query.edit_message_text(f'❇️ Muy bien, pasame el texto del nuevo mensaje (o cancelá con /cancelar):')

    return MENSAJE_AGREGAR

@restricted
def get_agregar_mensaje(update, context):
    # si usamos text_markdown_v2 a secas no se arman previews de los links
    # text_markdown_v2 - https://python-telegram-bot.readthedocs.io/en/stable/telegram.message.html#telegram.Message.text_markdown_v2
    text = update.message.text_markdown_v2_urled

    msg = get_msg_guita()
    Texto.create(mensaje=msg, contenido=text)

    keyboard = [common_ending_keyboard()]
    reply_markup = InlineKeyboardMarkup(keyboard)

    update.message.reply_text(f'✅ Nuevo texto agregado:\n{text}', parse_mode='MarkdownV2', reply_markup=reply_markup)

    return CONVERSATIONS.FALLBACK

@restricted
def eliminar_mensaje(update, context):
    msg = get_msg_guita()

    texto_i = context.chat_data['mensaje_i']

    # eliminamos
    msg.textos[texto_i].delete_instance()

    # una vez eliminado tenemos que reacomodar last_sent_texto ya que este campo
    # es un índice del array por ende se corre si eliminamos un ítem anterior
    if texto_i <= msg.last_sent_texto:
        new_last_sent_texto = msg.last_sent_texto-1
        # esto ocurre si eliminamos el texto 0 y este era también last_sent_texto
        if new_last_sent_texto < 0:
            new_last_sent_texto = len(msg.textos)-1
        msg.last_sent_texto = new_last_sent_texto
        msg.save()

    keyboard = [common_ending_keyboard()]
    reply_markup = InlineKeyboardMarkup(keyboard)

    query = update.callback_query
    query.answer()
    query.edit_message_text(f'✅ Texto eliminado', reply_markup=reply_markup)

    return MENSAJE_OPERAR

@restricted
def editar_mensaje(update, context):
    msg = get_msg_guita()
    reply_text = escape_markdown_entities('❇️ Muy bien, te dejo el texto acá abajo así lo podés copiar. Ahora pasame el nuevo texto del mensaje (o cancelá con /cancelar).\n')
    reply_text = reply_text + msg.textos[context.chat_data['mensaje_i']].contenido

    query = update.callback_query
    query.answer()
    query.edit_message_text(reply_text, parse_mode='MarkdownV2')

    return MENSAJE_EDITAR

@restricted
def settear_proximo_mensaje(update, context):
    mensaje_i = context.chat_data['mensaje_i']

    msg = get_msg_guita()
    msg.last_sent_texto = mensaje_i - 1 if mensaje_i != 0 else len(msg.textos)-1
    msg.save()

    keyboard = [common_ending_keyboard()]
    reply_markup = InlineKeyboardMarkup(keyboard)

    query = update.callback_query
    query.answer()
    query.edit_message_text(f'✅ Mensaje seteado como próximo', parse_mode='MarkdownV2', reply_markup=reply_markup)

    return CONVERSATIONS.FALLBACK

@restricted
def get_editar_mensaje(update, context):
    # si usamos text_markdown_v2 a secas no se arman previews de los links
    # text_markdown_v2 - https://python-telegram-bot.readthedocs.io/en/stable/telegram.message.html#telegram.Message.text_markdown_v2
    text = update.message.text_markdown_v2_urled

    msg = get_msg_guita()
    texto = msg.textos[context.chat_data['mensaje_i']]
    texto.contenido = text
    texto.save()

    keyboard = [common_ending_keyboard()]
    reply_markup = InlineKeyboardMarkup(keyboard)

    update.message.reply_text(f'✅ Texto editado con éxito a:\n{text}', parse_mode='MarkdownV2', reply_markup=reply_markup)

    return CONVERSATIONS.FALLBACK
