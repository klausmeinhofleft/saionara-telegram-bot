import datetime, re

from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import (
    CommandHandler,
    CallbackQueryHandler,
    ConversationHandler,
    CallbackContext,
    MessageHandler,
    Filters,
)

from bot.constants import CONVERSATIONS
from bot.db import get_msg_guita
from bot.job import restart_msg_job
from bot.utils import restricted
from .cancel import cancel

DAYS, DAYS_WEEK, DAYS_TIME = CONVERSATIONS.create_consts(3)
# estas son variables de los botones, NO de los states
# le ponemos namespace casero con "_"
DAYS, TIME, RETURN, END = [f'{DAYS}_{i}' for i in range(4)]

time_re = re.compile('^([01][0-9]|2[0-3]|[0-9]):([0-5][0-9]|[0-9])$')

def create_handler():
    return ConversationHandler(
        entry_points=[CommandHandler('dias_horario', start_command)],
        states={
            DAYS: [
                CallbackQueryHandler(set_days, pattern='^' + DAYS + '$'),
                CallbackQueryHandler(set_time, pattern='^' + TIME + '$'),
            ],
            DAYS_WEEK: [
                CallbackQueryHandler(toggle_day, pattern='^d[0-9]$'),
            ],
            DAYS_TIME: [
                MessageHandler((Filters.text & ~Filters.command), get_input_time),
            ],
        },
        fallbacks=[
            CallbackQueryHandler(start_command, pattern='^' + RETURN + '$'),
            CallbackQueryHandler(end, pattern='^' + END + '$'),
            CommandHandler('cancelar', cancel),
        ],
    )

def end(update, context):
    # CallbackQuery - https://python-telegram-bot.readthedocs.io/en/stable/telegram.callbackquery.html
    query = update.callback_query
    query.answer()
    query.edit_message_text('❇️ Operación finalizada')
    return ConversationHandler.END

@restricted
def start_command(update, context):
    # InlineKeyboardButton -  https://python-telegram-bot.readthedocs.io/en/stable/telegram.inlinekeyboardbutton.html
    keyboard = [
        [
            InlineKeyboardButton("Setear días", callback_data=DAYS),
            InlineKeyboardButton("Setear horario", callback_data=TIME),
        ],
        [InlineKeyboardButton("Terminar", callback_data=END)],
    ]

    reply_markup = InlineKeyboardMarkup(keyboard)
    reply_text = '⏺ Qué deseás hacer?'

    # diferenciamos si viene por entry point o por un "volver"
    if update.message:
        update.message.reply_text(reply_text, reply_markup=reply_markup)
    else:
        query = update.callback_query
        query.answer()
        query.edit_message_text(reply_text, reply_markup=reply_markup)

    return DAYS


def _set_days(update, context):
    msg = get_msg_guita()
    msg_days = [d == '1' for d in msg.dias]

    days = ['D', 'L', 'M', 'M', 'J', 'V', 'S']

    keyboard = []
    days_keyboard = []
    for i, day in enumerate(days):
        text = f'({day})' if msg_days[i] else day
        days_keyboard.append(InlineKeyboardButton(text, callback_data=f'd{i}'))
    keyboard.append(days_keyboard)
    keyboard.append([InlineKeyboardButton("<< Volver", callback_data=RETURN)])

    reply_markup = InlineKeyboardMarkup(keyboard)

    query = update.callback_query
    query.answer()
    query.edit_message_text(f'❇️ Seleccioná los días que querés des/activar. Los días activos se ven entre paréntesis.', reply_markup=reply_markup)

    return DAYS_WEEK

@restricted
def set_days(update, context):
    return _set_days(update, context)

@restricted
def toggle_day(update, context):
    query = update.callback_query
    day = int(query.data[1:])

    msg = get_msg_guita()
    msg_split = [d for d in msg.dias]
    msg_split[day] = '1' if msg_split[day] == '0' else '0'
    msg.dias = ''.join(msg_split)
    msg.save()

    restart_msg_job(context)

    return _set_days(update, context)

@restricted
def set_time(update, context):
    query = update.callback_query
    query.answer()
    query.edit_message_text(f'❇️ Muy bien, pasame el horario en el formato HH:MM (de 00:00 a 23:59)')

    return DAYS_TIME

@restricted
def get_input_time(update, context):
    time = update.message.text

    match = time_re.match(time)
    if not match:
        update.message.reply_text('✴️ Horario inválido. Corregí y volvé a intentar:')
        return

    groups = [int(g) for g in match.groups()]
    horas, minutos = groups[0], groups[1]
    msg = get_msg_guita()
    msg.horario = datetime.time(horas, minutos, 0)
    msg.save()

    restart_msg_job(context)

    # pasamos de int a str y ponemos un cero adelante si es de un solo caracter
    horario_str = f'{str(horas).rjust(2,"0")}:{str(minutos).rjust(2,"0")}'

    keyboard = [
        [
            InlineKeyboardButton("<< Volver", callback_data=RETURN),
            InlineKeyboardButton("Terminar", callback_data=END)
        ]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)

    # devolvemos
    update.message.reply_text(f'✅ Horario configurado a las {horario_str}', reply_markup=reply_markup)

    return CONVERSATIONS.FALLBACK
